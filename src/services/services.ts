import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:3000/",
  withCredentials: false,
  headers: {
    "content-type": "application/json",
  },
});

export default {
  getDisease() {
    return apiClient.get("/diseases");
  },
  postDisease(data: any) {
    return apiClient.post("/diseases", data);
  },
  updateDisease(data: any) {
    return apiClient.put(`/diseases/${data._id}`, data);
  },
  deleteDisease(data: any) {
    return apiClient.delete(`/diseases/${data._id}`);
  },
  postAction(data: any) {
    return apiClient.post(`/diseases/${data.diseaseID}/actions`, data);
  },
  updateAction(data: any) {
    return apiClient.put(
      `/diseases/${data.diseaseID}/actions/${data._id}`,
      data,
    );
  },
  deleteAction(data: any) {
    return apiClient.delete(`/diseases/${data.diseaseID}/actions/${data._id}`);
  },
  getWiki() {
    return apiClient.get("/wikis");
  },
  postWiki(data: any) {
    return apiClient.post("/wikis", data);
  },
};
