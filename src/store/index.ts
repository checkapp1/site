import Vue from "vue";
import Vuex from "vuex";
import Vuelidate from "vuelidate";
import requestService from "@/services/services";

Vue.use(Vuex);
Vue.use(Vuelidate);

export default new Vuex.Store({
  state: {
    action: {},
    diseases: [],
    actionComponentShowState: "hide",
  },
  mutations: {
    MUTATE_ACTION(state, actionData) {
      state.action = actionData;
    },
    MUTATE_DISEASES(state, diseasesData) {
      state.diseases = diseasesData;
    },
    MUTATE_ACTION_COMPONENT_MODE(state, newMode) {
      state.actionComponentShowState = newMode;
    },
  },
  actions: {
    mutateAction({ commit }, { disease, action: actionData }) {
      if (!actionData) {
        actionData = {
          diseaseID: disease._id,
          _id: "",
          name: "",
          disclaimer: "",
          targetProfiles: [{}],
          wikiID: "",
        };

        commit("MUTATE_ACTION", {
          ...actionData,
        });
        commit("MUTATE_ACTION_COMPONENT_MODE", "new");
      } else {
        commit("MUTATE_ACTION", {
          ...actionData,
          diseaseID: disease._id,
        });
        commit("MUTATE_ACTION_COMPONENT_MODE", "edit");
      }
    },
    async mutateDiseases({ commit }) {
      const { data } = await requestService.getDisease();

      commit(
        "MUTATE_DISEASES",
        data.diseases.map((item: any) => {
          return { ...item, editMode: false };
        }),
      );
    },
    mutateActionComponentMode({ commit }, mode) {
      commit("MUTATE_ACTION_COMPONENT_MODE", mode);
    },
    resetAction({ commit }) {
      commit("MUTATE_ACTION", {});
    },
  },
  getters: {
    getAction: state => {
      return state.action;
    },
    getDiseases: state => {
      return state.diseases;
    },
    getMode: state => {
      return state.actionComponentShowState;
    },
  },
  modules: {},
});
