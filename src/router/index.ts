import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Diseases from "../views/Diseases.vue";
import Wikis from "../views/Wikis.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/diseases",
    name: "Diseases",
    component: Diseases,
  },
  {
    path: "/wikis",
    name: "Wikis",
    component: Wikis,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
