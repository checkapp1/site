import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueSwal from "vue-swal";
import CKEditor from "@ckeditor/ckeditor5-vue";
import "./assets/styles/index.css";

Vue.config.productionTip = false;

Vue.use(VueSwal);
Vue.use(CKEditor);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
