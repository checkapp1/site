FROM node:13.8.0-alpine3.11

ENV PORT=8080

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

RUN yarn install

COPY . ./

EXPOSE ${PORT}

CMD yarn serve --port ${PORT}
